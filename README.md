# buecherei-be

A collection of backend features for a library frontend application

## Technical Design
Gateway Server: Nest.js
Retrieval Server: Golang
Database: PostgreSQL

## License
MIT License - Copyright (c) 2021 Neslihan Genctürk

Please check LICENSE.md to view the license

Please check NOTICE.md to view the licenses of the immediate project dependencies
