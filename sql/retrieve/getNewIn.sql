SELECT *
FROM books
WHERE 
	(EXTRACT(MONTH FROM book_added_at)) = 7
	AND (EXTRACT(YEAR FROM book_added_at)) = 2021
	-- AND section_id = ? 
ORDER BY book_added_at DESC
LIMIT 100