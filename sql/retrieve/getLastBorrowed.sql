SELECT * -- todo: base case should not provide the user_id 
FROM borrowed
INNER JOIN books
ON borrowed.book_id = books.book_id
WHERE
	(EXTRACT(MONTH FROM book_added_at)) = 7
	AND (EXTRACT(YEAR FROM book_added_at)) = 2021
ORDER BY reserved_at DESC
LIMIT 100