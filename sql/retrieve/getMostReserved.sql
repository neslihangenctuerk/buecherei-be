SELECT * -- todo: base case should not provide the user_id 
FROM reservations
INNER JOIN books
ON reservations.book_id = books.book_id
WHERE
	(EXTRACT(MONTH FROM book_added_at)) = 7
	AND (EXTRACT(YEAR FROM book_added_at)) = 2021
	-- AND section_id = ? 
ORDER BY reserved_at DESC
GROUP BY book_id
LIMIT 100