CREATE TABLE sections(
	section_name VARCHAR(100) NOT NULL,
	section_abbreviation VARCHAR(5) NOT NULL,
	section_size INT,
	section_id SERIAL PRIMARY KEY
)