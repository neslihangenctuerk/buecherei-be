CREATE TABLE books(
	book_author VARCHAR(150) NOT NULL,
	book_title VARCHAR(250) NOT NULL,
    book_universal_id INT NOT NULL,
    book_release_date DATE NOT NULL,
	book_added_at DATE NOT NULL,
	book_id SERIAL PRIMARY KEY,
	section_id INT REFERENCES sections(section_id)
)