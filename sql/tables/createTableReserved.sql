CREATE TABLE reservations(
	user_id INT REFERENCES users(user_id) NOT NULL,
	book_id INT REFERENCES books(book_id) NOT NULL,
	reserved_at DATE NOT NULL,
	reservation_id SERIAL PRIMARY KEY
)