CREATE TABLE users(
    user_firstname VARCHAR(30) NOT NULL,
    user_lastname VARCHAR(30) NOT NULL,
    user_username VARCHAR(30) NOT NULL,
    user_hash VARCHAR NOT NULL,
    user_address VARCHAR(250) NOT NULL,
    user_birthday DATE NOT NULL,
    user_id SERIAL PRIMARY KEY
)