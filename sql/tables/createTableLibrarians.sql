CREATE TABLE librarians(
	librarian_firstname VARCHAR(30) NOT NULL,
    librarian_lastname VARCHAR(30) NOT NULL,
    librarian_username VARCHAR(15) NOT NULL UNIQUE,
	librarian_responsible_for INT REFERENCES sections(section_id),
	librarian_id SERIAL PRIMARY KEY
)