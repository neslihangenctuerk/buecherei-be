CREATE TABLE borrowed(
	user_id INT REFERENCES users(user_id) NOT NULL,
	book_id INT REFERENCES books(book_id) NOT NULL,
    borrowed_at DATE NOT NULL,
    extensions INT NOT NULL,
	id SERIAL PRIMARY KEY
)